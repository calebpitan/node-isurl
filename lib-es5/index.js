"use strict";

var hasToStringTag = require("has-to-string-tag-x");

var isObject = require("is-object");

var isURLSearchParams = require("is-urlsearchparams");

var _require = require("./props"),
    lenientProperties = _require.lenientProperties,
    strictProperties = _require.strictProperties;

var searchParams = "searchParams";
var toStringTag = Object.prototype.toString;
var urlClass = "[object URL]";

var isURL = function isURL(url) {
  var supportIncomplete = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

  if (!isObject(url)) {
    return false;
  } else if (hasToStringTag && toStringTag.call(url) !== urlClass) {
    // Shimmed implementation with incorrect constructor name
    return false;
  } else if (!strictProperties.every(function (prop) {
    return prop in url;
  })) {
    return false;
  } else if (supportIncomplete && searchParams in url) {
    return isURLSearchParams.lenient(url.searchParams);
  } else if (supportIncomplete) {
    return true;
  } else if (lenientProperties.every(function (prop) {
    return prop in url;
  })) {
    return isURLSearchParams(url.searchParams);
  } else {
    return false;
  }
};

isURL.lenient = function (url) {
  return isURL(url, true);
};

module.exports = isURL;
//# sourceMappingURL=index.js.map